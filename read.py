# -*- coding: utf-8 -*-
"""
Enables reading of radiosonde data taken from the Integrated Global Radiosonde
Archive (IGRA) database into pandas DataFrame format.

"""
import pandas as pd

# column specifications for the fixed-width sonde files provided by NOAA
COL_SPECS = {
    'LVLTYP1': (0, 1),
    'LVLTYP2': (1, 2),
    'ETIME': (3, 8),
    'PRESS': (9, 15),
    'PFLAG': (15, 16), 
    'GPH': (16, 21),
    'ZFLAG': (21, 22),
    'TEMP': (22, 27),
    'TFLAG': (27, 28),
    'RH': (28, 33),
    'DPDP': (34, 39),
    'WDIR': (40, 45),
    'WSPD': (46, 51)
}

# the columns of the resulting read-in dataframe
COLUMNS = ['DATE'] + list(COL_SPECS.keys())

# list of tuples specifying the column number range for each feature
COL_WIDTHS = list(COL_SPECS.values())

# pressure levels to preserve (in pa) (IGRA archive is pretty limited in the 
# pressure levels it offers)
PRESS_LEVELS = [100000, 92500, 85000, 70000, 50000, 40000, 20000]


def _clean_sonde_df(df):
    """
    Removes flagged data, and trims data back to desired fields and pressure 
    levels, once data has been read into a pandas.DataFrame from an IGRA .txt
    file.
    
    """
    # trim df to relevant columns
    df = df[['DATE', 'PRESS', 'TEMP', 'DPDP', 'WDIR', 'WSPD']]
    # trim df to relevant pressure levels
    df = df[df['PRESS'].isin(PRESS_LEVELS)]
    # remove flagged entries from df
    mask = (
        (df['TEMP'] > -8888) & 
        (df['DPDP'] > -8888) & 
        (df['WDIR'] > -8888) & 
        (df['WSPD'] > -8888)
    )
    df = df[mask]   
    # reset the index
    df = df.reset_index(drop=True)
    return df


def read_sonde_file(path):
    """
    Reads the data from a single radiosonde .txt file from the IGRA database 
    into a pandas DataFrame. The .txt file will need to first be manually 
    downloaded from the IGRA database. Due to the format of these files,
    reading-in has to be done manually rather than using one of pandas' 
    predefined read functions, and so this process can be quite inefficient 
    for large files.
    
    Args:
        path (str): path to the downloaded IGRA radiosonde .txt file.
        
    Returns:
        df (pandas.DataFrame): the contents of `path` read into a pandas 
            DataFrame. This data has not been cleaned.
            
    """
    sondes = []
    with open(path) as f:
        # loop through every line of the .txt file, and split it by whitespace 
        # into a list
        for line in f:
            # lines beginning with '#' indicate the start of a new sonde
            if line[0] == '#':
                # for line beginning with '#', infer the date. If inferred hour 
                # then the data has been flagged, so skip it
                data = line.split()
                hour = int(data[4])
                if hour != 99:
                    date = pd.to_datetime(
                        f"{data[1]}{data[2]}{data[3]}{hour}", format='%Y%m%d%H'
                    )
                else:
                    date = False
            # if line doesn't begin with '#', then it is regular sonde data
            else:
                # if the date hasn't been flagged, then the date and the data
                data = [date]
                for index in COL_WIDTHS:
                    item = line[index[0]:index[1]]
                    # most elements are numeric
                    try:
                        item = int(item)
                    # for those that aren't, cut away the whitespace and leave
                    # as a string
                    except:
                        item = item.replace(' ', '')
                    data.append(item)
                # append new line of data to `sondes` once fully read
                sondes.append(data)
    # once all lines have been read, pack the contents into a dataframe
    df = pd.DataFrame(sondes, columns=COLUMNS)
    # clean resulting df
    df = _clean_sonde_df(df)
    return df
