# -*- coding: utf-8 -*-
"""
This module provides the tools needed to undertake Expectation-Maximisation
cluster analysis of a radiosonde dataset.

"""
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.mixture import GaussianMixture
from synotype.process import FEATURES


def fit_model(data, n_components):
    """
    This function fits a scaler and Gaussian Mixture model to the raw feature
    data contained in `data`. It is expected that `data` is unmodified output 
    of the `make_features` function in the `process` module. The current 
    function returns a tuple containing a fitted scaler and Gaussian Mixture 
    model respectively, both of which are then used in subsequent functions to
    predict cluster labels of the original or new data.
    
    Args: 
        data (pandas.DataFrame): feature data which has been produced by the
            function `make_features` contained in the `process` module.
        n_components (int): the number of gaussians (clusters) to be identified
            in the data.
            
    Returns:
        scaler (sklearn.preprocessing.StandardScaler): the scaler used to
            normalise `data` prior to running the analysis.
        model (sklearn.mixture.GaussianMixture): the gaussian mixture model
            which has been fitted to the normalised form of `data`, containing
            `n_components` gaussians (clusters).
            
    """
    # scale the features
    scaler = StandardScaler()
    scaler.fit(data)
    data_scaled = scaler.transform(data)
    # run the gaussian mixtures model, predict label for each sample
    gaumix = GaussianMixture(n_components=n_components)
    model = gaumix.fit(data_scaled)
    return scaler, model


def predict_labels(data, scaler, model, as_probs=False, labels_only=False):
    """
    Predicts the label for each sample in `data` based on the fitted `scaler`
    and `model` args, as provided by the `fit_model` function.
    
    Args:
        data (pandas.DataFrame): feature data which has been produced by the
            function `make_features` contained in the `process` module, for
            which labels are to be predicted. This could be the same data 
            already fed into the `fit_model`, or completely new data.
        scaler (sklearn.preprocessing.StandardScaler): the fitted scaler
            returned by the `fit_model` function.
        model (sklearn.mixture.GaussianMixture): the fitted Gaussian Mixture
            model returned by the `fit_model` function.
        as_probs (bool, default False): specifies whether a single label or 
            label probabilities are assigned to each sample in the output.
        labels_only (bool , default False): specifies whether only the label 
            data or the entire dataset for each sample is returned in the 
            output.
            
    Returns:
        data (pandas.DataFrame): a dataframe containing each sample from the 
            original `data` input with assigned labels or label probabilities.

    """
    data = data.copy()
    # scale current data based on the scaler used for the original model
    data_scaled = scaler.transform(data)
    # predict the labels or label prob for each samples, and attach this to 
    # the original data
    if as_probs:
        labels = model.predict_proba(data_scaled)
        columns = []
        for i in range(np.shape(labels)[1]):
            columns.append(f'PROB_LABEL_{i}')
        new_data = pd.DataFrame(labels, columns=columns, index=data.index)
        data = pd.concat([data, new_data], axis=1)
    else:
        labels = model.predict(data_scaled)
        data['LABEL'] = labels 
    # remove unwanted data fields
    if labels_only:
        data.drop(columns=FEATURES, inplace=True)
    data = np.round(data, 2)
    return data


def label_means(data, winds_to_polar=False):
    """
    Determines the mean features for each label, for a set of classified data.
    
    Args:
        data (pandas.DataFrame): classified data, containing assigned labels 
            (not probabilities!) for each sample in a 'LABEL' column, as well 
            as feature data for each sample (this data is directly produced by 
            `predict_labels` using that function's default settings).
            
        winds_to_polar (bool, default False): converts wind vector data back to
            meteorological-style directions and magnitudes (where the
            direction corresponds to the upstream direction of the wind).
            
    Returns:
        means (pandas.DataFrame): dataframe containing the feature means for 
            each label, indexed by label.
        
    """
    means = data.groupby('LABEL').mean()
    if winds_to_polar:
        for level in ['HIGH', 'MID', 'LOW']:
            columns = [f'{level}_UVEC', f'{level}_VVEC']
            wind_u = means[columns[0]]
            wind_v = means[columns[1]]
            means[f'{level}_MAG'] = np.sqrt(wind_u ** 2 + wind_v ** 2)
            # 
            means[f'{level}_DIR'] = (
                (180 / np.pi * np.arctan2(wind_u, wind_v) + 180) % 360
            )
        means.drop(columns=columns, inplace=True)
    means = np.round(means, 2)
    return means


def label_freqs(data):
    """
    Determines the frequency of occurrence of each label in a set of classified 
    data.
    
    Args:
        data (pandas.DataFrame): classified data, containing assigned labels 
            (not probabilities!) for each sample in a 'LABEL' column.
            
    Returns:
        freqs (pandas.Series): contains the frequency of occurrence of each
            label in the classified data.
        
    """
    size = len(data)
    counts = data.groupby('LABEL').size()
    freqs = np.round(counts / size, 3)
    return freqs


def k_nearest_neighbours(sample, data, k):
    """
    Finds the k nearest neighbours to `sample` from all sondes in `data`, based 
    on Euclidean distance. Assumes sonde data has been pre-processed by the 
    `process.make_features` function. Also assumes `sample` is a subset (single
    sample) of `data`.
    
    Args:
        sample (pandas.Series): a single sample of sonde feature data, whose
            nearest neighbours are to be found.
        data (pandas.DataFrame): the sonde feature dataset from which the 
            nearest neighbours are found.
        k (positive int): the number of nearest neighbours to be returned.
            
    Returns:
        result (pandas.DataFrame): a dataframe of sonde feature data containing
            `k` samples, corresponding to the `k` nearest sondes to `sample` 
            based on scaled features. Sorted in ascending order, with the most
            similar sonde as the first entry.
            
    """
    # scale the features
    scaler = StandardScaler()
    scaler.fit(data)
    sample_scaled = scaler.transform(sample.values.reshape(1, -1))
    data_scaled = scaler.transform(data)
    # calculate euclidean distance between the sample and each data point
    distance = np.sqrt(((data_scaled - sample_scaled) ** 2).sum(axis=1))
    distance = pd.Series(distance, index=data.index)
    # sort distances in ascending order, and return the first k not including
    # the sample itself
    distance.sort_values(inplace=True)
    dates = distance.index[1:k+1]
    result = data.loc[dates]
    return result
