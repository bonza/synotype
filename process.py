# -*- coding: utf-8 -*-
"""
This module provides the function `make_features`, which converts radiosonde
data into a set of features ready for ingestion by the Expectation-Maximisation
clustering algorithm.

"""
import numpy as np
import pandas as pd
from synotype.read import PRESS_LEVELS

# the colun names for the final features of the data
FEATURES = [
    'HIGH_DD', 'MID_DD', 'LOW_DD', 'HIGH_UVEC', 'MID_UVEC', 'LOW_UVEC', 
    'HIGH_VVEC', 'MID_VVEC', 'LOW_VVEC'
]

# threshold pressures used to create pressure level bins
PRESS_DIVS = [199., 499., 849., 1000.]


def _scale_data(df):
    """
    Scales the fields in `df` (a radiosonde dataset read from an IGRA .txt 
    file) so that they represent normal units.
        
    """
    # necessary scaling for each affected field (these values can be inferred 
    # from the IGRA website)
    scale_factors = {'PRESS': 100., 'TEMP': 10., 'DPDP': 10., 'WSPD': 10.}
    for col in scale_factors :
        df[col] /= scale_factors[col]
    return df


def _vectorise_winds(df):
    """
    Converts wind directional and magnitude data in `df` into horizontal (u) 
    and vertical (v) components. Assumes the input directional data is in 
    degrees, with the given direction representing where the wind is coming
    from (the upwind direction). In contrast, the resulting vectors point
    towards the downwind direction.
        
    """
    # resulting vectors will point in the downwind direction
    df['UVEC'] = np.round(-df['WSPD'] * np.sin(np.pi * df['WDIR'] / 180.), 2)
    df['VVEC'] = np.round(-df['WSPD'] * np.cos(np.pi * df['WDIR'] / 180.), 2)
    return df


def make_features(df, min_date=None, hours=None, months=None, raw=False):
    """
    This function converts the original sonde data contained in `df` into a
    set of features ready for analysis. Specifically, it divides data from 
    each separate sonde flight into low-level, mid-level, and high-level
    sections, based on pressure. It then takes the mean dewpoint depression,
    u vector component, and v vector component for each of these three levels,
    totalling nine features for each individual sonde flight.
    
    Args:
        df (pandas.DataFrame): a dataframe containing radiosonde samples, as
            returned by the `read_sonde_df` function in the `read` module.
        min_date (str): the date before which data from `df` is discarded, in
            format 'YYYY-MM-DD'. If None, all data is preserved.
        hours (list of int, default None): nominal radiosonde flight hours for
            which data in `df` are preserved. If None, all data is preserved.
        months (list of int, default None): the months for which radiosonde 
            data is preserved. If None, all data is preserved.
        raw (bool): option to return a dataframe containing features for each
            individual pressure level, with each date corresponding to a single
            sample.
            
    Returns:
        features (pandas.DataFrame): the feature data for each sonde.
        
    """
    if min_date:
        df = df[df['DATE'] >= min_date]        
    # trim df to desired hour and months (latter if specified)
    dates = pd.DatetimeIndex(df['DATE'])
    # initialise a mask of all True
    mask = dates == dates
    if hours:
        mask &= dates.hour.isin(hours)
    if months:
        mask &= dates.month.isin(months)
    df = df[mask]
    # scale each feature  of df to normal units
    df = _scale_data(df)
    # create u and v wind vector components
    df = _vectorise_winds(df)
    # trim df to only dates where sufficient data exists (i.e. all levels)
    date_groups = df.groupby('DATE')
    sizes = date_groups.size()
    sizes = sizes[sizes == len(PRESS_LEVELS)]
    dates = sizes.index
    df = df[df['DATE'].isin(dates)]
    # if `raw` is specified, return a dataframe of individual levels
    if raw:
        df['PRESS'] = df['PRESS'].astype(int).astype(str)
        multi = pd.MultiIndex.from_frame(df[['DATE', 'PRESS']])
        cols = ['DPDP', 'UVEC', 'VVEC']
        data = df[cols].values
        df = pd.DataFrame(data, index=multi, columns=cols)
        df = df.unstack(level=-1)
        df.columns = ['_'.join(col) for col in df.columns.values]
        return df
    # create pressure bins for the data, and assign each sample to a bin
    bins = pd.IntervalIndex.from_breaks(PRESS_DIVS, closed='right')
    bins = pd.cut(df['PRESS'], bins)
    bins.name = 'PRESS_BIN'
    df = pd.concat([df, bins], axis=1)
    # trim df back to just the base features (dewpoint depression and wind 
    # vectors), and those needed for grouping (dates and pressure bins)
    df = df[['DATE', 'PRESS_BIN', 'DPDP', 'UVEC', 'VVEC']]
    features = df.groupby(['DATE', 'PRESS_BIN']).mean()
    # create new columns for each pressure bin sub-index, and rename columns
    features = features.unstack()
    features.columns = FEATURES
    return features