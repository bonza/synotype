# About

The `synotype` package enables synoptic typing and classification of radiosonde 
data using the Expectation-Maximisation Clustering Algorithm.

# Getting radiosonde data

It's up to the user to download the radiosonde dataset they want to analyse
(this should be for a single location per analysis). This data needs to be 
downloaded from NOAA's Integrated Global Radiosonde Archive (IGRA), located at:

https://www.ncdc.noaa.gov/data-access/weather-balloon/integrated-global-radiosonde-archive 

This archive contains a single text (.txt) file per sonde location, which
contains all the historical sonde data for that location. Navigating this archive
is fairly self-explanatory, but if you get stuck there is some good 
documentation on the website to help you along, including a sonde location
look-up directory. Once you've found the file you want, simply download it to 
your local drive (this might take a while, as the files can often be quite large).
After this, no further interaction with the file is required, as `synotype` will 
carry out all reading and cleaning of the data prior to analysis.

# Usage

In a Python session, do the following steps.

1. Import the `synotype` package: 
    ```
    import synotype as st
    ```

2. Read in the radiosonde .txt file of interest to a `pandas.DataFrame`:
    ```
    df = st.read.read_sonde_file(path/to/file.txt)
    ```
    
3. Convert this raw sonde data into a set of features suitable for analysis (a 
number of options are available with this function, see the doc for 
details):
    ```
    features = st.process.make_features(df)
    ```

4. Specify the representative number of clusters you expect to exist in the data, 
and fit a scaler and Gaussian Mixtures (EM) model:
    ```
    n_components = 5
    scaler, model = st.analyse.fit_model(features, n_components)
    ```
    
5. Finally, predict the cluster labels of your features (or new) dataset based 
on the generated model (again, a number of options are available here, including 
returning label probabilities for each sample, as opposed to a single assigned 
label):
    ```
    labels = st.analyse.predict_labels(features, scaler, model)
    ```
    
That's it. In the above example, the `labels` output is a DataFrame with a single 
'LABEL' column containing assigned integer labels, and indexed by radiosonde date.

You'll need to explore the results to figure out which synoptic set-up
each label corresponds to. Due to random initialisation of the Gaussian Mixtures
algorithm, re-running the above steps from scratch will result in approximately
the same clusters but with the integer labels in a different order. Therefore, 
if you intend to map these labels to actual synoptic descriptors (e.g. 'Monsoon'), 
it would be wise to save the `scaler` and `model` objects using `pickle`, and 
then re-use these pickled instances, and the associated label-to-name mapping, 
in each subsequent use.